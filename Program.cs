﻿using Newtonsoft.Json;
using ParserSupport;
using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.ComponentModel;
using AegisImplicitMail;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.NetworkInformation;
using PuppeteerSharp;
using System.Xml.Linq;

namespace Parser
{
    public class  QueryFileHeader
    {
        public int storessearchcount;
        public QueryFileHeader() {}
        public QueryFileHeader(int value)
        {
            storessearchcount = value;
        }
    }
    public class QueryFileStoreLink
    {
        public int storeindex;
        public string link;
        public QueryFileStoreLink() { }
        public QueryFileStoreLink(int stindx, String lnk)
        {
            storeindex=stindx; 
            link = lnk;
        }
    }
    public class QueryStoresInfo : QueryFileStoreLink
    {
        public int lastprice;
        public int currentprice;
        public DateTime lastdate;
        public QueryStoresInfo() : base() { }
        public QueryStoresInfo(int stindx, String lnk, int lastprc, int crntprc, DateTime dt) : base(stindx, lnk) 
        {
            storeindex = stindx;
            link = lnk;
            lastprice = lastprc;
            currentprice = crntprc;
            lastdate = dt;
        }
    }

    public class QueryFilePriceRecord
    {
        public int storeindex;
        public int price;
        public QueryFilePriceRecord() { }
        public QueryFilePriceRecord(int indx, int prc) 
        {
            this.storeindex = indx;
            this.price = prc;
        }
    }    
    public class Query
    {
        public string text;
        public QueryFileHeader storescount;
        public QueryStoresInfo[] querystoresarray;
        public string[] emailinglist;
    }

    class Program
    {
        static DateTime currentdatetime;
        static byte storecnt;
        static InternetStore[] storesarray;
        static byte[] colomnsposition;
        static Query[] queries;
        static byte currentquery;


        struct InternetStoreOld
        {
            public byte index;
            public string name;
            public string website;
            public string linkforsearching;          
            public string priceparam;
            public string priceparam2;
            public char divider;
            public char divider2;
            public byte predividercount;
            public byte predividercount2;
        }


        struct InternetStore
        {
            public byte index;
            public string name;
            public string website;
            public string linkforsearching;
            public WebsitePriceParsingData[] parseData;
        }
        /*public struct WebsitePriceParsingData
        {
            public string priceparam;
            public char divider;
            public byte predividercount;
        }*/

            const byte APPMODE_CONSOLE = 0;
        const byte APPMODE_BCKGRND = 1;
        const string queriesfolder = "queries\\";
        public byte appmode = 0;
        static Boolean showpricemassages = false;
        static Boolean sendemail = false;
        static Boolean performsilentcheck = false;

        static void Init()
        {
            if (!CheckInternetConnection())
                CloseApp("There is no internet connection.");
            currentdatetime = DateTime.Now;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("uk-UA");
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            Directory.SetCurrentDirectory(System.AppContext.BaseDirectory);
            LoadStoresInfo();
            var queryfiles = Directory.EnumerateFiles(System.AppContext.BaseDirectory + queriesfolder, "*.query", SearchOption.AllDirectories).ToArray();
            var tempquery = new Query();
            if (queryfiles.Count() > 0)
            {
                queries = new Query[1];
                queries[0] = new Query();
            }
            else
                CloseApp("No one query file was found.");
            for (int i = 0; i <= queryfiles.Count() - 1; i++)
            {
                if (LoadQueryFile(queryfiles[i], ref queries[queries.Length - 1], true))
                {
                    Array.Resize<Query>(ref queries, queries.Length + 1);
                    queries[queries.Length - 1] = new Query();
                }
            }
            Array.Resize<Query>(ref queries, queries.Length - 1);
            currentquery = 0;
        }
        
        static Boolean CheckInternetConnection()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }          
        }
        
        static void LoadStoresInfo()
        {          
            try
            {              
                storesarray = JsonConvert.DeserializeObject<InternetStore[]>(File.ReadAllText("Storesinfo.info"));
                storecnt = (byte) storesarray.Length;
             }                                         
            catch (IOException exc)
            {
                CloseApp("Storesinfo.info loading error:\n" + exc.Message);
            }
        }
        static Boolean LoadQueryFile(string filename, ref Query query, Boolean createPricesTXT)
        {
            query.text = Path.GetFileName(queriesfolder + filename);
            query.text = query.text.Remove(query.text.Length - 6);
            string s;
            DateTime dt;
            QueryFilePriceRecord[] qfpr;
            int[] baseQfpr;
            int i = 0;         
            try
            {
                var FileIn = new StreamReader(new FileStream(filename, FileMode.Open));
                String pricesTXT="";
                try
                {
                    query.storescount = JsonConvert.DeserializeObject<QueryFileHeader>(FileIn.ReadLine());
                    query.querystoresarray = new QueryStoresInfo[query.storescount.storessearchcount];
                    for (i = 0; i <= query.storescount.storessearchcount - 1; i++)
                        query.querystoresarray[i] = JsonConvert.DeserializeObject<QueryStoresInfo>(FileIn.ReadLine());
                    query.emailinglist = JsonConvert.DeserializeObject<string[]>(FileIn.ReadLine());
                    if (createPricesTXT)                 
                        pricesTXT += QueryStoresNamesToStr(query) + "\n";
                    baseQfpr = new int[query.storescount.storessearchcount];
                    while ((s = FileIn.ReadLine()) != null)
                    {
                        dt = JsonConvert.DeserializeObject<DateTime>(s);                       
                        qfpr = JsonConvert.DeserializeObject<QueryFilePriceRecord[]>(FileIn.ReadLine());                                                                                                                                                                                           
                        for (int j = 0; j <= qfpr.Length - 1; j++)
                        {
                            for (int ii = 0; ii <= query.querystoresarray.Length - 1; ii++)
                                if (query.querystoresarray[ii].storeindex == qfpr[j].storeindex)
                                {
                                    query.querystoresarray[ii].lastdate = dt;
                                    query.querystoresarray[ii].lastprice = qfpr[j].price;                                   
                                    if (createPricesTXT)                                   
                                        if (qfpr[j].price<0) baseQfpr[ii]=0;
                                            else baseQfpr[ii] = qfpr[j].price;                                                                          
                                }
                        }
                        if (createPricesTXT) pricesTXT += String.Format("{0,-22}", dt) + QueryStoresPricesToStr(baseQfpr) + "\n";
                    }
                    if (createPricesTXT) File.WriteAllText(queriesfolder+query.text+".txt", pricesTXT);
                }
                catch (JsonException excjs)
                {
                    FileIn.Close();
                    Console.WriteLine(Path.GetFileName(filename) + " contains an error:\n" + excjs.Message);
                    Thread.Sleep(2000);
                    return false;
                }
                FileIn.Close();
                return true;
            }
            catch (IOException exc)
            {
                Console.WriteLine(exc.Message);
                Thread.Sleep(2000);
                return false;
            }
        }
                        
        static string QueryStoresNamesToStr(Query q)
        {
            String s = String.Format("{0,-22}", "DateTime");

            for (int i = 0; i <= q.storescount.storessearchcount - 1; i++)
                s += String.Format("{0,-20}", storesarray[q.querystoresarray[i].storeindex].name);
            return s;
        }
        static string QueryStoresPricesToStr(int[] qfpr)
        {
            String s = "";
            //String.Format("Name = {0}, hours = {1:hh}", name, DateTime.Now);
            for (int i = 0; i <= qfpr.Length - 1; i++)
                s += String.Format("{0,-20}", qfpr[i]);
            return s;
        }

        static void GetQueryCurrentPrices(Query qr, Boolean show)
        {
            Console.Clear();
            for (int i = 0; i <= qr.storescount.storessearchcount - 1; i++)
            {
                qr.querystoresarray[i].currentprice = GetPrice(qr.querystoresarray[i]);
                if (show) ShowLoadingProcess("Obtaining info from internet", (byte)Math.Round((double)(i + 1) * 100 / qr.storescount.storessearchcount));
            }
            Console.Clear();
        }
        static void ShowQueryInfo()
        {
            Console.WriteLine(currentdatetime.ToString() + "   Price check for: " + queries[currentquery].text + "\n");
            colomnsposition = new byte[] { 0, 20, 32, 46 };
            DrawTableHeader(colomnsposition[0], colomnsposition[1], colomnsposition[2], colomnsposition[3]);
            int[] sorted = SortQueryArray(queries[currentquery]);
            for (int i = 0; i <= queries[currentquery].storescount.storessearchcount - 1; i++)
                ShowQueryStoreInfo(queries[currentquery].querystoresarray[sorted[i]], colomnsposition[0], colomnsposition[1], colomnsposition[2], colomnsposition[3]);
            for (int i = 0; i <= queries[currentquery].emailinglist.Length - 1; i++)
                Console.Write(queries[currentquery].emailinglist[i] + "  ");
            Console.WriteLine();

        }
        static void DrawTableHeader(byte NmClmn, byte PrcClmn, byte LstPrcClmn, byte LstDtClmn)
        {
            Console.SetCursorPosition(NmClmn, Console.CursorTop);
            Console.Write("Store");
            Console.SetCursorPosition(PrcClmn, Console.CursorTop);
            Console.Write("Price,grn");
            Console.SetCursorPosition(LstPrcClmn, Console.CursorTop);
            Console.Write("Prev.price");
            Console.SetCursorPosition(LstDtClmn, Console.CursorTop);
            Console.WriteLine("Date");
            for (int i = 1; i <= LstDtClmn + 10; i++) Console.Write("-"); Console.WriteLine();
        }
        static void ShowQueryStoreInfo(QueryStoresInfo qsinf, byte NmClmn, byte PrcClmn, byte LstPrcClmn, byte LstDtClmn)
        {
            Console.SetCursorPosition(NmClmn, Console.CursorTop);
            Console.Write(storesarray[qsinf.storeindex].name);
            Console.SetCursorPosition(PrcClmn, Console.CursorTop);
            if (qsinf.currentprice < qsinf.lastprice & qsinf.currentprice>0)
                Console.ForegroundColor = System.ConsoleColor.Green;
            else if (qsinf.currentprice > qsinf.lastprice & qsinf.currentprice > 0)
                Console.ForegroundColor = System.ConsoleColor.Red;
            Console.Write(CheckPriceForError(qsinf.currentprice, true));
            Console.ResetColor();
            Console.SetCursorPosition(LstPrcClmn, Console.CursorTop);
            Console.Write(CheckPriceForError(qsinf.lastprice, true));
            Console.SetCursorPosition(LstDtClmn, Console.CursorTop);
            Console.WriteLine(qsinf.lastdate.ToString("dd.MM.yyyy"));
        }
        static string CheckPriceForError (int price, Boolean shortMessage)
        {
            if (price < 0) return ParserSupport.PriceParsing.GetParsingErrorMessage(price, shortMessage);
            else return price.ToString();
        }

        static void ChkPriceDrps(byte query)
        {
            string msg = "The price dropped in store(s):\n";
            Boolean dropped = false;
            Boolean changes = false;
            QueryFilePriceRecord[] qfpr = new QueryFilePriceRecord[0];
            for (int i = 0; i <= queries[query].storescount.storessearchcount - 1; i++)
                if (queries[query].querystoresarray[i].currentprice != queries[query].querystoresarray[i].lastprice /*& 
                    queries[query].querystoresarray[i].currentprice>-1 & queries[query].querystoresarray[i].lastprice>-1*/)
                {
                    if (!changes) changes = true;
                    if (queries[query].querystoresarray[i].currentprice < queries[query].querystoresarray[i].lastprice 
                        & queries[query].querystoresarray[i].currentprice > 0)
                    {
                        if (!dropped) dropped = true;
                        msg += "\"" + storesarray[queries[query].querystoresarray[i].storeindex].name + "\"" + " from " +
                            queries[query].querystoresarray[i].lastprice + " to " + queries[query].querystoresarray[i].currentprice + "\n";
                    }
                    queries[query].querystoresarray[i].lastdate = currentdatetime;
                    Array.Resize(ref qfpr, qfpr.Length + 1);
                    qfpr[qfpr.Length - 1] = new QueryFilePriceRecord(queries[query].querystoresarray[i].storeindex, queries[query].querystoresarray[i].currentprice);
                }
            if (changes) AddQueryNewPrices(queries[query].text + ".query", qfpr);
            if (dropped & sendemail)
                for (byte i = 0; i <= queries[query].emailinglist.Length - 1; i++)
                    SendEmail("Price drop checker for " + queries[query].text, CreatHTMLEmail(queries[query]), queries[query].emailinglist[i]);
            if (dropped & showpricemassages & !performsilentcheck) User32dllMessageBox.MessageBox((IntPtr)0, msg, "Price checker", 0);

        }
        static void AddQueryNewPrices(string filename, QueryFilePriceRecord[] rcd)
        {
            try
            {
                var FileOut = new StreamWriter(new FileStream(queriesfolder + filename, FileMode.Append));
                FileOut.WriteLine(JsonConvert.SerializeObject(currentdatetime));
                FileOut.WriteLine(JsonConvert.SerializeObject(rcd));
                FileOut.Flush();
                FileOut.Close();
            }
            catch (IOException exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        static void ShowLoadingProcess(string processname, byte processstate)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(processname + ": " + processstate + " %");
        }

        static void CloseApp(string text)
        {
            Console.Clear();
            Console.WriteLine(text + "\n Press any key to exit...");
            Thread.Sleep(5000);
            System.Environment.Exit(0);
        }
        static int GetPrice(QueryStoresInfo qinf)
        {
            var prpar = new PriceParsing(qinf.link, storesarray[qinf.storeindex].parseData);
            int price = prpar.ParsePrice();
            if (price == -1)
                if (!CheckInternetConnection())
                    CloseApp("There's no Internet connection. Exiting.");
            return price;            
        }
        static int GetPrice(string link, int storeindex)
        {
            var prpar = new PriceParsing(link, storesarray[storeindex].parseData);
            int price = prpar.ParsePrice();
            return price;
        }


        static string ReadHTMPFromFile(string path)
        {
            try
            {
                return File.ReadAllText(path);
            }
            catch (IOException exc)
            {
                Console.WriteLine(exc.Message);
                return "HTML file opening error ";
            }
        }

        static void CreatQueryFile(Query q)
        {
            try
            {
                var FileOut = new StreamWriter(new FileStream(queriesfolder + q.text + ".query", FileMode.Create));
                FileOut.WriteLine(JsonConvert.SerializeObject(new QueryFileHeader(q.storescount.storessearchcount)));
                for (int i = 0; i <= q.storescount.storessearchcount - 1; i++)
                    FileOut.WriteLine(JsonConvert.SerializeObject(new QueryFileStoreLink(q.querystoresarray[i].storeindex, q.querystoresarray[i].link)));
                FileOut.WriteLine(JsonConvert.SerializeObject(q.emailinglist));
                FileOut.WriteLine(JsonConvert.SerializeObject(currentdatetime));
                QueryFilePriceRecord[] qfpr = new QueryFilePriceRecord[q.storescount.storessearchcount];
                for (int i = 0; i <= q.storescount.storessearchcount - 1; i++)
                    qfpr[i] = new QueryFilePriceRecord(q.querystoresarray[i].storeindex, q.querystoresarray[i].lastprice);
                FileOut.WriteLine(JsonConvert.SerializeObject(qfpr));
                FileOut.Flush();
                FileOut.Close();
            }
            catch (IOException exc)
            {
                Console.WriteLine(exc.Message);
                Thread.Sleep(3000);
            }
        }
        
        static Boolean CheckNewStoreRequest()
        { return false; }



        public static void OpenBrowser(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }
                else
                {
                    throw;
                }
            }
        }

        static void NewSearchRequest()
        {
            Console.Clear();
            Query newquery = new Query();
            InputDataFromConsole(ref newquery.text, Console.CursorLeft, Console.CursorTop, 50, "Enter new request string and press 'Enter':", newStringAfter: true);
            newquery.storescount = new QueryFileHeader();
            newquery.storescount.storessearchcount = 0;
            String newreqeststring = newquery.text.Replace(" ", "+");
            for (int i = 0; i <= storecnt - 1; i++)
            {
                //Console.WriteLine("Press '1' to check \"" + storesarray[i].name + "\" store. Then, copy target items link. Press '2' for skipping this store:");
                Char ch = default(char);//ConsoleKeyInfo kpr = default(ConsoleKeyInfo);// = Console.ReadKey(false);
                InputDataFromConsole(ref ch, Console.CursorLeft, Console.CursorTop, 1, "Press '1' to check \"" + storesarray[i].name + "\" store. Then, copy target items link. Press '2' for skipping this store:", PressKeyMode.InputDigitKey, true);
                if (ch == '2') continue;
                OpenBrowser(storesarray[i].linkforsearching + newreqeststring);
                //Console.Write("Did you find target item (1 - yes, 2 - no):");
                //kpr = Console.ReadKey(false);
                //Console.WriteLine();
                InputDataFromConsole(ref ch, Console.CursorLeft, Console.CursorTop, 1, "Did you find target item (1 - yes, 2 - no):", PressKeyMode.InputDigitKey, true);
                if (ch == '1')
                {
                    if (newquery.storescount.storessearchcount == 0)
                    {
                        newquery.querystoresarray = new QueryStoresInfo[1];
                        newquery.querystoresarray[0] = new QueryStoresInfo();
                    }
                    else
                    {
                        Array.Resize(ref newquery.querystoresarray, newquery.querystoresarray.Length + 1);
                        newquery.querystoresarray[newquery.querystoresarray.Length - 1] = new QueryStoresInfo();
                    }
                    newquery.storescount.storessearchcount++;
                    Console.WriteLine("Please, copy here the webpage link:");
                    newquery.querystoresarray[newquery.storescount.storessearchcount - 1].link = Console.In.ReadLine();
                    newquery.querystoresarray[newquery.storescount.storessearchcount - 1].storeindex = i;
                    Console.Write("Getting price info from the website...");
                    newquery.querystoresarray[newquery.storescount.storessearchcount - 1].lastprice = GetPrice(newquery.querystoresarray[newquery.storescount.storessearchcount - 1]);
                    newquery.querystoresarray[newquery.storescount.storessearchcount - 1].lastdate = currentdatetime;
                    if (newquery.querystoresarray[newquery.storescount.storessearchcount - 1].lastprice < 0) Console.WriteLine("Failed");
                    else Console.WriteLine("OK");
                }
                else continue;
            }
            if (newquery.storescount.storessearchcount > 0)
            {
                Console.Write("Enter emailing adress(s) in format \"email1\", \"email2\"...: ");
                string emailstr = Console.ReadLine();
                emailstr = "[" + emailstr + "]";
                try
                {
                    string[] emailarray = JsonConvert.DeserializeObject<string[]>(emailstr);
                    newquery.emailinglist = new string[emailarray.Length];
                    for (byte i = 0; i <= emailarray.Length - 1; i++)
                        newquery.emailinglist[i] = emailarray[i];
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                CreatQueryFile(newquery);
                Init();
            }
            Console.Clear();
        }

        static void MainMenuShow()
        {
            Console.Clear();
            Console.WriteLine("Choose the option 1-{0}:", queries.Length + 2);
            Console.WriteLine("1. Create new search request on known store's webpages");
            int i;
            for (i = 0; i <= queries.Length - 1; i++)
                Console.WriteLine(2 + i + ". Open \"" + queries[i].text + "\" query");
            Console.WriteLine(i + 2 + ". Exit");
            Console.Write("Enter number:");
        }

        static void MainMenu()
        {
            MainMenuShow();
            int cursortoppos = Console.CursorTop;
            int cursorleftpos = Console.CursorLeft;
            Boolean enterednumberok = false;
            byte optionnum = 0;
            char ch = ' ';
            #if DEBUG           
                File.WriteAllText("queries_debugging.txt", "");
            #endif
            do
            {
                InputDataFromConsole(ref optionnum, Console.CursorLeft, Console.CursorTop, 2, "");
                if (optionnum >= 1 & optionnum <= queries.Length + 2)
                {
                    if (optionnum == queries.Length + 2)
                    {
                        System.Environment.Exit(0);
                    }
                    if (optionnum == 1)
                    {
                        NewSearchRequest();
                        MainMenuShow();
                    }
                    else
                    {
                        currentquery = (byte)(optionnum - 2);
                        #if DEBUG
                            File.AppendAllText("queries_debugging.txt", "Start debugging "+ queries[currentquery].text+"\n");
                        #endif
                        GetQueryCurrentPrices(queries[currentquery], true);
                        #if DEBUG
                            File.AppendAllText("queries_debugging.txt", "End debugging " + queries[currentquery].text + "\n");
                        #endif
                            ShowQueryInfo();
                        ChkPriceDrps(currentquery);
                        Console.WriteLine();
                        InputDataFromConsole(ref ch, Console.CursorLeft, Console.CursorTop, 1, "Press 'm' for mailing current query or any key to back in the menu...", PressKeyMode.InputAnyKey);
                        if (ch == 'm'^ch=='ь')
                            for (byte i = 0; i <= queries[currentquery].emailinglist.Length - 1; i++)
                                SendEmail("Price drop checker for " + queries[currentquery].text, CreatHTMLEmail(queries[currentquery]), queries[currentquery].emailinglist[i]);
                        MainMenuShow();
                    }
                }
                else
                {
                    Console.CursorLeft = cursorleftpos;
                    Console.Write("   ");
                    Console.CursorLeft = cursorleftpos;
                }
            }
            while (!enterednumberok);
        }

        static void SilentQueriesCheck()
        {
            ConsoleVisibility.Visible(false);
            for (byte i = 0; i <= queries.Length - 1; i++)
            {
                GetQueryCurrentPrices(queries[i], true);
                ChkPriceDrps(i);
            }
        }

        static void CheckAppParams(string[] prms)
        {
            if (prms.Length > 0)
                for (byte i = 0; i <= prms.Length - 1; i++)
                    switch (prms[i])
                    {
                        case "?":
                            CloseApp("showpricemassages - show popup window for query if price(s) have droppped\nsendemail - turn on emailing when price dropped\nsilentcheck - perforce check all the queries in background");
                            break;
                        case "showmassage":
                            showpricemassages = true;
                            break;
                        case "sendemail":
                            sendemail = true;
                            break;
                        case "silentcheck":
                            performsilentcheck = true;
                            break;
                        default:
                            CloseApp("Invalid parameter '" + prms[i] + "'. Use:\n'silentcheck' for shadow check all the queries\n'showmassage' to show price drop windows\n'sendemail' to turn on price drop emailing");
                            break;
                    }
            if (performsilentcheck)
            {
                SilentQueriesCheck();
                System.Environment.Exit(0);
            }
        }

        public static void SendEmail(string subject, string body, string emailTo)
        {
            var mail = "tarry@ukr.net";
            var host = "smtp.ukr.net";
            var user = "tarry@ukr.net";
            var pass = "LtZo58DOC7loX3Hi";
            var mymessage = new MimeMailMessage();
            mymessage.From = new MimeMailAddress(mail);
            mymessage.To.Add(emailTo);
            mymessage.Subject = subject;
            mymessage.Body = body;
            mymessage.IsBodyHtml = true;
            var mailer = new MimeMailer(host, 465);
            mailer.User = user;
            mailer.Password = pass;
            mailer.SslType = SslMode.Ssl;
            mailer.AuthenticationMode = AuthenticationType.Base64;
            mailer.Send(mymessage);
            Thread.Sleep(5000);
        }
        
        static int[] SortQueryArray (Query q)
        {
            var order = new int[q.querystoresarray.Length];
            var sortparam = new int[q.querystoresarray.Length];
            
            for (int i = 0; i <= q.querystoresarray.Length - 1; i++)
                order[i] = i;                      
                       
            int failprices = 0;
            for (int p = 0; p <= q.querystoresarray.Length - 1; p++)
                if (q.querystoresarray[p].currentprice <= 0) failprices++;
            if (failprices== q.querystoresarray.Length)
                return order;
            
            for (int i = 0; i <= q.querystoresarray.Length - 1; i++)             
                sortparam[i] = q.querystoresarray[i].currentprice;
            int posToChange = GetLastGoodPricePos(sortparam, sortparam.Length-1);
            
            for (int i=0;i<= posToChange-1;i++)
                if (sortparam[i]<=0)
                {
                    SwapIntVars(ref sortparam[i], ref sortparam[posToChange]);
                    SwapIntVars(ref order[i], ref order[posToChange]);
                    posToChange = GetLastGoodPricePos(sortparam, posToChange-1);
                }           

            for (int i = 0; i <= posToChange; i++)
                for (int j = i + 1; j <= posToChange; j++)
                    if (sortparam[i] > sortparam[j])
                    {
                        SwapIntVars(ref sortparam[i],ref sortparam[j]);
                        SwapIntVars(ref order[i], ref order[j]);                       
                    } 
            return order;
        }

        static void SwapIntVars(ref int a, ref int b)
        { int tmp = a; a = b; b = tmp; }
         static int GetLastGoodPricePos(int[] arr, int startPos)
        {
            for (int i = startPos; i >= 0; i--)
                if (arr[i] > 0) return i;
            return 0;
        }
      
                    
        static string CreatHTMLEmail(Query q)
        {
            int[] sorted = SortQueryArray(q);
            string he = "<html> <h1><pre>&nbsp;" + q.text + "</h1>"+
                  "<TABLE border=\"0\"  width=400> <TR bgcolor=#d0ffd6 align=center height=30>" +
                  "<TD>Store</TD> <TD>Price,GRN</TD>" +
                  "<TD style=color:grey>Prev.price</TD>" +
                  "<TD>Link</TD> </TR> ";
            for (int i = 0; i <= q.querystoresarray.Length - 1; i++)
            {
                
                he += "<TR align=center> <TD align=left height=20>" + storesarray[q.querystoresarray[sorted[i]].storeindex].name +"</TD>";
                if (q.querystoresarray[sorted[i]].currentprice < q.querystoresarray[sorted[i]].lastprice & q.querystoresarray[sorted[i]].currentprice > 0)
                    he += "<TD  style=color:green><STRONG>";
                else if (q.querystoresarray[sorted[i]].currentprice > q.querystoresarray[sorted[i]].lastprice & q.querystoresarray[sorted[i]].currentprice>0)
                        he += "<TD  style=color:red>";
                else he += "<TD>";
                //if (q.querystoresarray[sorted[i]].currentprice<0) he += "" + "0";
                /*else*/ he += "" + CheckPriceForError(q.querystoresarray[sorted[i]].currentprice, true); 
                if (q.querystoresarray[sorted[i]].currentprice < q.querystoresarray[sorted[i]].lastprice)
                    he += "</STRONG>";
                he +="</TD>";
                he += "<TD style=color:gray>" + CheckPriceForError(q.querystoresarray[sorted[i]].lastprice,true) + "</TD>";
                he += "<TD><a href=\"" + q.querystoresarray[sorted[i]].link + "\">item link</a></TD> </TR>";
            }
            he += "</TABLE>";
            he += "<p>&nbsp;</p>";           
            he += "<div>Software ver. " + System.Reflection.Assembly.GetEntryAssembly().GetName().Version + "</div>";
            return he;
        }

        public class User32dllMessageBox
        {
            [DllImport("User32.dll", CharSet = CharSet.Unicode)]
            public static extern int MessageBox(IntPtr h, string m, string c, int type);
        }

        public enum InputModeType
        {
            InputAnyText = 0,
            InputDecimateDigits = 1,
            InputFloatDigits = 2,
            InputDigitChar = 3,
            InputLetterChar = 4,
            InputLetterAndDigitChar = 5,
            InputAnyChar = 6
        }
        public enum PressKeyMode
        {
            InputByEnter = 0,
            InputDigitKey = 1,
            InputLetterKey = 2,
            InputDigitOrDigitKey = 3,
            InputAnyKey = 4
        }

        static void InputDataFromConsole(ref string data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            if (pressKeyMode > 0)
                data = InputText((InputModeType)(pressKeyMode + 2), curX, curY, 1, prompt, pressKeyMode, newStringAfter);
            else
                data = InputText(InputModeType.InputAnyText, curX, curY, maxLength, prompt, pressKeyMode, newStringAfter);
        }
        static void InputDataFromConsole(ref char data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            if (pressKeyMode > 0)
                data = Convert.ToChar(InputText((InputModeType)(pressKeyMode + 2), curX, curY, 1, prompt, pressKeyMode, newStringAfter));
            else
                data = Convert.ToChar(InputText(InputModeType.InputAnyText, curX, curY, 1, prompt, pressKeyMode, newStringAfter));
        }
        static void InputDataFromConsole(ref int data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            data = Int32.Parse(InputText(InputModeType.InputDecimateDigits, curX, curY, maxLength, prompt, pressKeyMode, newStringAfter));
        }
        static void InputDataFromConsole(ref byte data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            if (pressKeyMode == PressKeyMode.InputDigitKey)
                data = byte.Parse(InputText(InputModeType.InputDigitChar, curX, curY, 1, prompt, pressKeyMode, newStringAfter));
            else
                data = byte.Parse(InputText(InputModeType.InputDecimateDigits, curX, curY, maxLength, prompt, pressKeyMode, newStringAfter));
        }
        static void InputDataFromConsole(ref double data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            data = double.Parse(InputText(InputModeType.InputFloatDigits, curX, curY, maxLength, prompt, pressKeyMode, newStringAfter));
        }
        static void InputDataFromConsole(ref float data, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode = 0, Boolean newStringAfter = false)
        {
            data = float.Parse(InputText(InputModeType.InputFloatDigits, curX, curY, maxLength, prompt, pressKeyMode, newStringAfter));
        }

        static string InputText(InputModeType inputmode, int curX, int curY, int maxLength, string prompt, PressKeyMode pressKeyMode, Boolean newStringAfter = false) 
        {
            string inpdatastr = "";
            Console.CursorLeft = curX;
            Console.CursorTop = curY;
            Console.Write(prompt);
            ConsoleKeyInfo key;
            Boolean inputOk = false;
            do
            {
                 key = Console.ReadKey(true);
                 if (inpdatastr.Length < maxLength)
                        switch (inputmode)
                        {
                            case InputModeType.InputAnyText:
                                if (Char.IsPunctuation(key.KeyChar) ^ Char.IsLetterOrDigit(key.KeyChar) ^ Char.IsSeparator(key.KeyChar))
                                {
                                    inpdatastr += key.KeyChar;
                                    Console.Write(key.KeyChar);
                                }
                                break;
                            case InputModeType.InputDecimateDigits:
                                if (Char.IsDigit(key.KeyChar))
                                {
                                    inpdatastr += key.KeyChar;
                                    Console.Write(key.KeyChar);
                                }
                                break;
                            case InputModeType.InputFloatDigits:
                                if (Char.IsDigit(key.KeyChar) ^ key.KeyChar == Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
                                {
                                    inpdatastr += key.KeyChar;
                                    Console.Write(key.KeyChar);
                                }
                                break;
                            case InputModeType.InputDigitChar:
                                if (Char.IsDigit(key.KeyChar))
                                {
                                    inpdatastr += key.KeyChar;
                                    Console.Write(key.KeyChar);
                                    if (newStringAfter) Console.WriteLine();
                                    return inpdatastr;
                                }
                                break;
                            case InputModeType.InputLetterChar:
                                if (Char.IsLetter(key.KeyChar))
                                {
                                    inpdatastr += key.KeyChar;
                                    Console.Write(key.KeyChar);
                                    if (newStringAfter) Console.WriteLine();
                                    return inpdatastr;
                                }
                                break;                           
                            case InputModeType.InputLetterAndDigitChar:
                            if (Char.IsLetterOrDigit(key.KeyChar))
                            {
                                inpdatastr += key.KeyChar;
                                Console.Write(key.KeyChar);
                                if (newStringAfter) Console.WriteLine();
                                return inpdatastr;
                            }
                            break;
                            case InputModeType.InputAnyChar:
                                inpdatastr += key.KeyChar;
                                Console.Write(key.KeyChar);
                                if (newStringAfter) Console.WriteLine();
                                return inpdatastr;
                            break;
                        default:
                                break;
                        }
                    switch (key.Key)
                    {
                        case ConsoleKey.Backspace:
                            if (inpdatastr.Length >= 1)
                            {
                                Console.CursorLeft--;
                                Console.Write(" ");
                                Console.CursorLeft--;
                                inpdatastr = inpdatastr.Substring(0, inpdatastr.Length - 1);
                            }
                            break;
                        case ConsoleKey.Enter:
                            if (inpdatastr.Length > 0 & inputmode<(InputModeType)3)
                                inputOk = true;
                            break;
                        default:
                            break;
                    }
            }
            while (!inputOk);
            if (newStringAfter) Console.WriteLine();
                return inpdatastr;
            }

        static void Main(string[] args)
        {
            Init();         
            CheckAppParams(args);
            MainMenu();
        }           
    } 
      
    class ConsoleVisibility
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        public static void Visible(Boolean visibility)
        {
            IntPtr handle = GetConsoleWindow();
            if (visibility) ShowWindow(handle, SW_SHOW);
            else ShowWindow(handle, SW_HIDE);
        }
    }    
    

}
